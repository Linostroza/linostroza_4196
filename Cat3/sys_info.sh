#!/bin/bash

# Script para llevar la salida del sistema a una página

TITLE="Sistema de informacion para la maquina $HOSTNAME"
CURRENT_TIME=$(date +"%x %r %Z")
TIMESTAMP="Generado en $CURRENT_TIME, by $USER"

report_user() {
    cat /etc/passwd | grep gamma30
    return
}

report_groups() {
     cat /etc/group | grep gamma30
    return
}

report_partition_1() {
    df -h
    return
}

report_dir_etc() {
    du -hlsc /etc
    return
}

report_mem_princ(){
    free -k
    return
}

echo "
<HTML>
    <HEAD>
        <TITLE>$TITLE</TITLE>
    </HEAD>
    <BODY>
        <H1>$TITLE</H1>
        <P>$TIMESTAMP</P>
        $(report_user)
        $(report_groups)
        $(report_partition_1)
        $(report_dir_etc)
        $(report_mem_princ)
    </BODY>
</HTML>"