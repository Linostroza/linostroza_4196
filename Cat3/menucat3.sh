#!/bin/bash
press_enter()
{
    echo -en "\nPuse Intro para Continuar"
    read
    clear
}
selection=
until [ "$selection" = "0" ]; do
    echo "
    MENU DE PROGRAMA
    1 - Crea Archivo  file1 y asigna permisos
    2 - Verifica si File1 es archivo o directorio

    0 - exit program
"
    echo -n "Ingrese Seleccion: "
    read selection
    echo ""
    case $selection in
        1 ) if [ -f $HOME\/file1 ]
	          then 
    	           echo " archivo file1 existe." 
                else 
                   echo "archivo file1 no existe.'"
                    ls  -l > $HOME\/file1
                	echo "file  creado en : "
                	cd ..
                	pwd
             	fi ;;
        2 )  if [ -f $HOME\/file1 ]
	          then 
    	           echo " archivo file1 existe." 
                           echo "asignando permisos rw- r----x"
                           chmod  u-x,g-wx,o-rw,o+x $HOME\/file1
                           cd $HOME
                           pwd 
                           ls -l | grep file1
                else 
                   echo "archivo file1 no existe.'"
             	fi ;;
        0 ) exit ;;
        * ) echo "Seleccione opcion  1, 2, or 0"; press_enter
    esac
done
